const fetch = require("node-fetch");

const dataGeneratorUrl = process.env.DATA_GENERATOR_URL;
const dataApiUrl = process.env.API_URL;

// Increase timeout as this might runs a little longer than the default
jest.setTimeout(10000);

describe("e2e test", () => {
  const exampleTimestamp = Date.now().toString();
  const exampleRequestBody = JSON.stringify({ timestamp: exampleTimestamp });

  let itemCountBefore;

  beforeEach(async () => {
    const responseBefore = await fetch(dataApiUrl, {
      method: "GET",
    });
    const itemsBefore = await responseBefore.json();
    itemCountBefore = itemsBefore.length;
  });

  describe("given a message is posted to the endpoint", () => {
    beforeEach(async () => {
      await fetch(dataGeneratorUrl, {
        method: "POST",
        body: exampleRequestBody,
        headers: { "Content-Type": "application/json" },
      });
    });

    it("processes the message", async () => {
      await new Promise((resolve) => setTimeout(resolve, 5000));

      const response = await fetch(dataApiUrl, {
        method: "GET",
      });
      const itemsAfter = await response.json();
      const itemCountAfter = itemsAfter.length;

      expect(itemCountBefore).toEqual(itemCountAfter - 1);
    });
  });
});
