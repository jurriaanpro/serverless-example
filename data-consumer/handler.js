const { DynamoDB } = require("aws-sdk");

const dynamodbClient = new DynamoDB();

const consumer = async (event) => {
  for (const record of event.Records) {
    const messageAttributes = record.messageAttributes;
    console.log(
      "Message Attribute: ",
      messageAttributes.AttributeName.stringValue
    );
    console.log("Message Body: ", record.body);

    const timestamp = Date.now().toString();

    const params = {
      TableName: process.env.DATABASE_NAME,
      Item: {
        PK: { S: "ITEM#" },
        SK: { S: timestamp },
        "record.body": { S: JSON.stringify(record.body) },
      },
    };

    await dynamodbClient.putItem(params).promise();
  }
};

module.exports = {
  consumer,
};
