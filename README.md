# serverless example

## Requirements

- node16
- Serverless Framework
- make

## Architecture

This application consists of multiple sub-components, they're described below. It's based on serverless components (AWS Lambda, DynamoDB and SQS).

![Architecture overview](architecture-overview.png)

### data-producer

Exposes an endpoint that when called, will post the request body to an SQS queue

### data-consumer

Triggers on messages posted to SQS queue and persists the data to a DynamoDB table

### api

Exposes an API endpoint to read the data from DynamoDB table

### shared

Contains shared resources that are used across functions, such as the DynamoDB table and SQS queue.

## IaC

- Serverless Framework
- Cloudformation

Serverless Framework has good support for deploying serverless application on AWS and other relevant resources can be configured using Cloudformation support.

## CI/CD

A basic Gitlab CI/CD pipeline has been setup that builds, tests and deploys the application.

## Monitoring/logging

Lumigo.io has been connected to the AWS account for monitoring and debugging the Lambda functions

![Lumigo screenshot](lumigo.png)

## Security

Using [`serverless-iam-roles-per-function`](https://www.npmjs.com/package/serverless-iam-roles-per-function) the principle of least privilege is applied. Only the IAM actions that the function should be able to perform are applied to the functions IAM policy document.

Possible extensions could be to limit incoming/outgoing traffic using Security Groups.

## Automated testing

### Unit testing

No unit testing has been added

### End-to-end testing

An example end-to-end test has been added that runs against a deployed application.

A proper setup would be to let CI create a temporary environment and let it run against this temporary environment.
