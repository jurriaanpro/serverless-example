"use strict";

const { DynamoDB } = require("aws-sdk");
const dynamodbClient = new DynamoDB();

module.exports.hello = async (event) => {
  const params = {
    TableName: process.env.DATABASE_NAME,
  };

  const response = await dynamodbClient.scan(params).promise();
  const items = response.Items;

  return {
    statusCode: 200,
    body: JSON.stringify(items),
  };
};
