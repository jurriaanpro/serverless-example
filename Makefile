.PHONY: build
build:
	echo "I would be triggering some npm script that builds the application(s)"

.PHONY: package
package:
	npx serverless package

.PHONY: deploy
deploy:
	npx serverless deploy

.PHONY: test
	echo "I would be triggering some npm script that tests the application(s)"

.PHONY: test-e2e
test-e2e:
	DATA_GENERATOR_URL=<dynamically resolve from deployment output> API_URL=<dynamically resolve from deployment output> npm run test:e2e
